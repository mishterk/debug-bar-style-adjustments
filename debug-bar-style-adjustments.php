<?php
/*
Plugin Name: Debug Bar Keyboard Shortcut
Plugin URI: https://bitbucket.org/mishtershmart/debug-bar-style-adjustments
Description: Adds some style adjustments to the debug bar.
Version: 0.1.1
Author: Phil Smart
Author URI: http://philsmart.me
License:
*/

add_action( 'admin_head', 'debugBarStyleAdjustments' );
add_action( 'wp_head', 'debugBarStyleAdjustments' );
function debugBarStyleAdjustments()
{
    ?>
    <style>
        #debug-bar-menu {
            overflow-y : auto !important;
        }

        #debug-menu-links li a {
            line-height : 26px;
        }
    </style>
    <?php
}